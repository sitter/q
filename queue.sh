#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

set -ex

env
curl -X POST mellon.nerdpol.ovh:8080/v1/queue/unstable/source/plasma-framework/project/$CI_PROJECT_ID/pipeline/$CI_PIPELINE_ID/time/$CI_PIPELINE_CREATED_AT
